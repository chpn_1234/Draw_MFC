﻿
// MFC_DRAWView.cpp: CMFCDRAWView 类的实现
//

#include "pch.h"
#include "framework.h"
// SHARED_HANDLERS 可以在实现预览、缩略图和搜索筛选器句柄的
// ATL 项目中进行定义，并允许与该项目共享文档代码。
#ifndef SHARED_HANDLERS
#include "MFC_DRAW.h"
#endif

#include "MFC_DRAWDoc.h"
#include "MFC_DRAWView.h"
#include "DlgShowSortAlgorithm.h"
#include "DlgDrawGeometry.h"
#include "DlgFindLeaks.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CMFCDRAWView

IMPLEMENT_DYNCREATE(CMFCDRAWView, CView)

BEGIN_MESSAGE_MAP(CMFCDRAWView, CView)
	// 标准打印命令
	ON_COMMAND(ID_FILE_PRINT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, &CView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, &CMFCDRAWView::OnFilePrintPreview)
	ON_WM_CONTEXTMENU()
	ON_WM_RBUTTONUP()
//	ON_UPDATE_COMMAND_UI(ID_FILE_PRINT_PREVIEW, &CMFCDRAWView::ON_HUA_HUA)
ON_WM_PAINT()
ON_WM_LBUTTONDOWN()
ON_WM_LBUTTONUP()
ON_WM_RBUTTONDOWN()
ON_WM_MOUSEMOVE()
//ON_COMMAND(IDR_SORT, &CMFCDRAWView::OnIdrSort)
ON_COMMAND(ID_ONSORT_DLG, &CMFCDRAWView::OnOnsortDlg)
ON_COMMAND(ID_ONDRAW_DLG, &CMFCDRAWView::OnOndrawDlg)
ON_COMMAND(ID_OPEN_DLG_FIND_LEAKS, &CMFCDRAWView::OnOpenDlgFindLeaks)
END_MESSAGE_MAP()

// CMFCDRAWView 构造/析构

CMFCDRAWView::CMFCDRAWView() noexcept
{
	// TODO: 在此处添加构造代码

	m_right_down = 0;
	m_mouse_bool = false;
}

CMFCDRAWView::~CMFCDRAWView()
{
}

BOOL CMFCDRAWView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: 在此处通过修改
	//  CREATESTRUCT cs 来修改窗口类或样式

	return CView::PreCreateWindow(cs);
}

// CMFCDRAWView 绘图

void CMFCDRAWView::OnDraw(CDC* pDC)
{
	CMFCDRAWDoc* pDoc = GetDocument();
	ASSERT_VALID(pDoc);
	if (!pDoc)
		return;

	// TODO: 在此处为本机数据添加绘制代码
}


// CMFCDRAWView 打印


void CMFCDRAWView::OnFilePrintPreview()
{
#ifndef SHARED_HANDLERS
	AFXPrintPreview(this);
#endif
}

BOOL CMFCDRAWView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// 默认准备
	return DoPreparePrinting(pInfo);
}

void CMFCDRAWView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加额外的打印前进行的初始化过程
}

void CMFCDRAWView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: 添加打印后进行的清理过程
}

void CMFCDRAWView::OnRButtonUp(UINT /* nFlags */, CPoint point)
{
	ClientToScreen(&point);
	OnContextMenu(this, point);
}

void CMFCDRAWView::OnContextMenu(CWnd* /* pWnd */, CPoint point)
{
#ifndef SHARED_HANDLERS
	theApp.GetContextMenuManager()->ShowPopupMenu(IDR_POPUP_EDIT, point.x, point.y, this, TRUE);
#endif
}


// CMFCDRAWView 诊断

#ifdef _DEBUG
void CMFCDRAWView::AssertValid() const
{
	CView::AssertValid();
}

void CMFCDRAWView::Dump(CDumpContext& dc) const
{
	CView::Dump(dc);
}

CMFCDRAWDoc* CMFCDRAWView::GetDocument() const // 非调试版本是内联的
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CMFCDRAWDoc)));
	return (CMFCDRAWDoc*)m_pDocument;
}
#endif //_DEBUG


// CMFCDRAWView 消息处理程序




void CMFCDRAWView::OnPaint()
{

	CPaintDC dc(this);               // device context for painting
									 // TODO: 在此处添加消息处理程序代码
									 // 不为绘图消息调用 CView::OnPaint()
	dc.Rectangle(100, 100, 300, 300);
}


void CMFCDRAWView::OnLButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	m_pt_origin  = point;
	m_last_point = point;
	m_mouse_bool = true;

	CView::OnLButtonDown(nFlags, point);
}


void CMFCDRAWView::OnLButtonUp(UINT nFlags, CPoint point)
{
	m_mouse_bool = false;

	// TODO: 在此添加消息处理程序代码和/或调用默认值
	////-----------方法1 使用CDC类实现----------
	//CPen pen(PS_DASH, 1, RGB(255, 0, 0));
	//CDC* pdc = GetDC();
	//CPen* p1d_pen = pdc->SelectObject(&pen);
	//pdc->MoveTo(m_pt_origin);
	//pdc->LineTo(point);
	//pdc->SelectObject(p1d_pen);
	//ReleaseDC(pdc);

	//--------------方法2 使用CCLientDC类实现-----------
	/*CPen pen(PS_SOLID, 10, RGB(0, 255, 0));
	CClientDC dc(this);
	CPen* p1d_pen = dc.SelectObject(&pen);
	dc.MoveTo(m_pt_origin);
	dc.LineTo(point);
	dc.SelectObject(p1d_pen);*/


	////-------------方法3使用CWindowsDC类实现-----------
	////创建画笔
	////CPen pen(PS_SOLID, 10, RGB(255, 0, 255));
	///*CPen pen(PS_DASH, 1, RGB(255, 0, 255));*/
	//CPen pen(PS_DOT, 1, RGB(255, 0, 255));

	////获取窗口设备指针
	//CWindowDC dc(this);
	///*CWindowDC dc(GetParent());*/
	///*CWindowDC dc(GetDesktopWindow());*/

	//CPen* p1d_pen = dc.SelectObject(&pen);
	////画线
	//dc.MoveTo(m_pt_origin);
	//dc.LineTo(point);
	//dc.SelectObject(p1d_pen);



	//--------------画刷---------
	/*CBrush brush(RGB(255,0,0));
	CClientDC dc(this);
	dc.FillRect(CRect(m_pt_origin, point), &brush);*/


	//------------画透明刷-------
	/*CBrush* p_brush= CBrush::FromHandle((HBRUSH)GetStockObject(NULL_BRUSH));
	CClientDC dc(this);
	CBrush* pold_brush=dc.SelectObject(p_brush);
	CPen pen(PS_SOLID, 5, RGB(255, 0, 0));
	CPen* pold_pen = dc.SelectObject(&pen);

	dc.Rectangle(CRect(m_pt_origin, point));
	dc.SelectObject(pold_brush);
	dc.SelectObject(pold_pen);*/

	//---------画圆角矩形----------
	////设置圆角
	//CPoint pt(30, 30);
	////设置矩形
	//CRect rc(m_pt_origin, point);
	//CClientDC dc(this);
	//CBrush*  p_brush   = CBrush::FromHandle((HBRUSH)GetStockObject(NULL_BRUSH));
	//CBrush* pold_brush = dc.SelectObject(p_brush);
 //	CPen pen(PS_SOLID, 10, RGB(255, 0, 0));
	//CPen* pold_pen = dc.SelectObject(&pen);
	//dc.RoundRect(&rc, pt);
	//dc.SelectObject(pold_pen);
	//dc.SelectObject(pold_brush);

	CView::OnLButtonUp(nFlags, point);
}


void CMFCDRAWView::OnRButtonDown(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	m_right_down++;
	CString str;
	str.Format(_T("the times of left button down = %d"), m_right_down);
	MessageBox(str);

	CView::OnRButtonDown(nFlags, point);
}


void CMFCDRAWView::OnMouseMove(UINT nFlags, CPoint point)
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	////--------------画扇形----------
	//CPen pen(PS_DOT,1, RGB(255, 0, 0));
	//CClientDC dc(this);
	//if (m_mouse_bool) {
	//	CPen* pold_pen = dc.SelectObject(&pen);
	//	dc.MoveTo(m_pt_origin);
	//	dc.LineTo(point);
	//	dc.LineTo(m_last_point);
	//	m_last_point = point;
	//	dc.SelectObject(pold_pen);
	//}

	//--------------画连续线----------
	CPen pen(PS_DOT,1, RGB(255, 0, 0));
	CClientDC dc(this);
	if (m_mouse_bool) {
		CPen* pold_pen = dc.SelectObject(&pen);
		dc.MoveTo(m_pt_origin);
		dc.LineTo(point);
		m_pt_origin = point;
		dc.SelectObject(pold_pen);
	}


	CView::OnMouseMove(nFlags, point);
}


void CMFCDRAWView::OnOnsortDlg()
{
	DlgShowSortAlgorithm dlg;
	dlg.DoModal();

}



void CMFCDRAWView::OnOndrawDlg()
{
	DlgDrawGeometry dlg;
	dlg.DoModal();
}


void CMFCDRAWView::OnOpenDlgFindLeaks()
{
	DlgFindLeaks dlg;
	dlg.DoModal();
}
