﻿// DlgFianLeaks.cpp: 实现文件
//

#include "pch.h"
#include "MFC_DRAW.h"
#include "DlgFindLeaks.h"
#include "afxdialogex.h"
#include <iostream>

char* CstringToCHar(CString str)
{
    USES_CONVERSION;
    str = _T("12 我当然 789");
    char* pChar = T2A(str);
    return pChar;
}

// DlgFianLeaks 对话框

IMPLEMENT_DYNAMIC(DlgFindLeaks, CDialogEx)

DlgFindLeaks::DlgFindLeaks(CWnd* pParent /*=nullptr*/)
    : CDialogEx(IDD_DLG_FIND_LEAKS, pParent)
{

}

DlgFindLeaks::~DlgFindLeaks()
{
}

void DlgFindLeaks::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange(pDX);
    DDX_Control(pDX, IDC_BUTTON_START_FIND_LEAKS, m_ctrStartFindLeaks);
    DDX_Control(pDX, IDC_EDIT_SHOW_LEAKS, m_ctrShowLeaks);
}


BEGIN_MESSAGE_MAP(DlgFindLeaks, CDialogEx)
    ON_BN_CLICKED(IDC_BUTTON_START_FIND_LEAKS, &DlgFindLeaks::OnBnClickedButtonStartFindLeaks)
END_MESSAGE_MAP()


// DlgFianLeaks 消息处理程序


void DlgFindLeaks::OnBnClickedButtonStartFindLeaks()
{
    int* pOneLeakPointer = new int[10];//创建一个泄露指针

    //---查找内存泄露位置

#define DEBUG

#ifndef DEBUG
#undef DEBUG
#endif // !DEBUG


    char* pChar2 ="567";

  
    for(int i=0; i<50; ++i)
    {
        pChar2 = CstringToCHar(_T("12 我当然 789"));

    }
}
