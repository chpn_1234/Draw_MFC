#include "pch.h"
#include <math.h>
#include <malloc.h>
#include <stdio.h>
#include <memory.h>
#include "BSpline.h"
#include <vector>
#include <iostream>
using namespace std;

void SVDslove(double** A, int m, int n, double** inverse)
{
	double	dEps = 0.000001f;
	double** V = (double**)malloc(m * sizeof(double*));
	double** M = (double**)malloc(n * sizeof(double*));
	double** U = (double**)malloc(n * sizeof(double*));
	double** tmp = (double**)malloc(n * sizeof(double*));
	double** eigen = (double**)malloc(n * sizeof(double*));
	double** p = (double**)malloc(n * sizeof(double*));
	double* sum = (double*)malloc(n * sizeof(double));
	int i, j, k, i_t, j_t, iter = 0;
	double max = 9999, tan2a, sina, cosa, alpha;

	for (i = 0; i < m; i++)
	{
		V[i] = (double*)malloc(n * sizeof(double));
	}

	for (i = 0; i < n; i++)
	{
		M[i] = (double*)malloc(n * sizeof(double));
		U[i] = (double*)malloc(n * sizeof(double));
		tmp[i] = (double*)malloc(n * sizeof(double));
		eigen[i] = (double*)malloc(n * sizeof(double));
		p[i] = (double*)malloc(n * sizeof(double));
	}

	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
		{
			M[i][j] = 0;
			eigen[i][j] = 0;
			for (k = 0; k < m; k++)
			{
				double a = A[k][i];
				double b = A[k][j];
				M[i][j] += A[k][i] * A[k][j];
				eigen[i][j] += A[k][i] * A[k][j];
			}
		}
	}

	iter = 0;

	while (max > dEps)
	{
		max = eigen[0][1];
		i_t = 0;
		j_t = 1;

		for (i = 0; i < n; i++)
		{
			for (j = i; j < n; j++)
			{
				if (j > i && fabs(eigen[i][j]) > max)
				{
					i_t = i;
					j_t = j;
					max = fabs(eigen[i][j]);
				}
			}
		}

		tan2a = 2 * eigen[i_t][j_t] / (eigen[i_t][i_t] - eigen[j_t][j_t]);
		alpha = atan(tan2a) / 2;
		sina = sin(alpha);
		cosa = cos(alpha);

		for (i = 0; i < n; i++)
		{
			for (j = 0; j < n; j++)
			{
				p[i][j] = 0;

			}
			p[i][i] = 1;
		}

		p[i_t][i_t] = cosa;
		p[j_t][j_t] = cosa;
		p[i_t][j_t] = -sina;
		p[j_t][i_t] = sina;

		for (i = 0; i < n; i++)
		{
			for (j = 0; j < n; j++)
			{
				tmp[i][j] = 0;
				for (k = 0; k < n; k++)
				{
					tmp[i][j] += p[k][i] * eigen[k][j];
				}
			}
		}

		for (i = 0; i < n; i++)
		{
			for (j = 0; j < n; j++)
			{
				eigen[i][j] = 0;
				for (k = 0; k < n; k++)
				{
					eigen[i][j] += tmp[i][k] * p[k][j];
				}
			}
		}

		for (i = 0; i < n; i++)
		{
			for (j = 0; j < n; j++)
			{
				if (iter == 0)
				{
					if (i == j)
					{
						tmp[i][j] = 1;
					}
					else
					{
						tmp[i][j] = 0;
					}
				}
				else
				{
					tmp[i][j] = U[i][j];
				}
			}
		}

		for (i = 0; i < n; i++)
		{
			for (j = 0; j < n; j++)
			{
				U[i][j] = 0;
				for (k = 0; k < n; k++)
				{
					U[i][j] += tmp[i][k] * p[k][j];
				}
			}
		}
		iter++;
	}

	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
		{
			if (i == j)
			{
				if (eigen[i][j] != 0)
				{
					eigen[i][j] = 1 / sqrt(eigen[i][j]);
				}
				else
				{
					eigen[i][j] = 0;
				}
			}
			else
			{
				eigen[i][j] = 0;
			}
		}
	}

	for (i = 0; i < n; i++)
	{
		sum[i] = 0;
		for (j = 0; j < n; j++)
		{
			sum[i] += U[j][i] * U[j][i];
		}
	}

	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
		{
			if (sum[j] != 0)
			{
				U[i][j] /= sqrt(sum[j]);
			}
			else
			{
				U[i][j] = 0;
			}
		}
	}

	for (i = 0; i < m; i++)
	{
		for (j = 0; j < n; j++)
		{
			V[i][j] = 0;
			for (k = 0; k < n; k++)
			{
				V[i][j] += A[i][k] * U[k][j];
			}
		}
	}

	for (i = 0; i < n; i++)
	{
		sum[i] = 0;
		for (j = 0; j < m; j++)
		{
			sum[i] += V[j][i] * V[j][i];
		}
	}

	for (i = 0; i < m; i++)
	{
		for (j = 0; j < n; j++)
		{
			if (sum[j] != 0)
			{
				V[i][j] /= sqrt(sum[j]);
			}
			else
			{
				V[i][j] = 0;
			}
		}
	}

	for (i = 0; i < n; i++)
	{
		for (j = 0; j < n; j++)
		{
			tmp[i][j] = 0;
			for (k = 0; k < n; k++)
			{
				tmp[i][j] += U[i][k] * eigen[k][j];
			}
		}
	}

	for (i = 0; i < n; i++)
	{
		for (j = 0; j < m; j++)
		{
			inverse[i][j] = 0;
			for (k = 0; k < n; k++)
			{
				inverse[i][j] += tmp[i][k] * V[j][k];
			}
		}
	}

	for (i = 0; i < m; i++)
	{
		free(V[i]);
	}

	for (i = 0; i < n; i++)
	{
		free(M[i]);
		free(U[i]);
		free(tmp[i]);
		free(eigen[i]);
		free(p[i]);
	}

	free(sum);
	free(V);
	free(M);
	free(U);
	free(tmp);
	free(eigen);
	free(p);
}

void Decompose_LU(double** M, int dim, double** L, double** U)
{
	int i, j, k;
	double sum;
	for (i = 0; i < dim; ++i)
	{
		L[i][i] = 1;
	}

	for (i = 0; i < dim; ++i)
	{
		for (j = i; j < dim; ++j)
		{
			sum = 0;
			for (k = 0; k < i; ++k)
			{
				sum += L[i][k] * U[k][j];
			}
			U[i][j] = (M[i][j] - sum) / L[i][i];
		}

		for (j = i + 1; j < dim; ++j)
		{
			sum = 0;
			for (k = 0; k < i; ++k)
			{
				sum += L[j][k] * U[k][i];
			}
			L[j][i] = (M[j][i] - sum) / U[i][i];
		}
	}
	sum = 0;
	for (i = 0; i < dim - 1; i++)
	{
		sum += L[dim - 1][i] * U[i][dim - 1];
	}
	U[dim - 1][dim - 1] = M[dim - 1][dim - 1] - sum;

	return;
}

void LU_Inverse(double** data, int dim, double** inverse)
{
	int i, j, k;
	double** L = (double**)malloc(dim * sizeof(double*));
	double** U = (double**)malloc(dim * sizeof(double*));
	double** L_inverse = (double**)malloc(dim * sizeof(double*));
	double** U_inverse = (double**)malloc(dim * sizeof(double*));

	for (i = 0; i < dim; ++i)
	{
		L[i] = (double*)calloc(dim, sizeof(double));
		U[i] = (double*)calloc(dim, sizeof(double));

		L_inverse[i] = (double*)malloc(dim * sizeof(double));
		U_inverse[i] = (double*)malloc(dim * sizeof(double));
	}

	Decompose_LU(data, dim, L, U);

	for (i = 0; i < dim; ++i)
	{
		for (j = 0; j < dim; ++j)
		{
			if (i == j)
			{
				L_inverse[i][i] = 1.0 / L[i][i];
				U_inverse[i][i] = 1.0 / U[i][i];
			}
			else if (i > j)
			{
				U_inverse[i][j] = 0;
			}
			else
			{
				L_inverse[i][j] = 0;
			}
		}
	}

	for (i = 0; i < dim - 1; ++i)
	{
		for (j = i + 1; j < dim; ++j)
		{
			if (j > i)
			{
				U_inverse[i][j] = 0;
				for (k = i; k < j; ++k)
				{
					U_inverse[i][j] += U_inverse[i][k] * U[k][j];
				}
				U_inverse[i][j] *= -U_inverse[j][j];
			}
		}
	}

	for (i = dim - 1; i >= 1; --i)
	{
		for (j = i - 1; j >= 0; --j)
		{
			if (i > j)
			{
				L_inverse[i][j] = 0;
				for (k = j + 1; k <= i; ++k)
				{
					L_inverse[i][j] += L_inverse[i][k] * L[k][j];
				}
				L_inverse[i][j] *= -L_inverse[j][j];
			}
		}
	}

	for (i = 0; i < dim; ++i)
	{
		for (j = 0; j < dim; ++j)
		{
			inverse[i][j] = 0;
			for (k = 0; k < dim; ++k)
			{
				inverse[i][j] += U_inverse[i][k] * L_inverse[k][j];
			}
		}
	}

	for (i = 0; i < dim; ++i)
	{
		free(L[i]);
		free(U[i]);
		free(L_inverse[i]);
		free(U_inverse[i]);
	}

	free(L);
	free(U);
	free(L_inverse);
	free(U_inverse);
}

double CaculateDistanceFunction(double* a, double* b, int size)
{
	double dis = 0;
	for (int i = 0; i < size; ++i)
	{
		dis += (b[i] - a[i]) * (b[i] - a[i]);
	}
	return sqrt(dis);
}


//------BSpline类------
BSpline::BSpline(Vect* InputData, int nInputNum, int nDim, int nDegree)
{
	m_nInputNum	= nInputNum;
	m_nDim		= nDim;
	m_nDegree	= nDegree;
	m_InputData = InputData;
	SplineParameterization(E_CHORD_PARAMETERIZED);//参数化输入点
	m_KnotData		= NULL;//节点
	m_ControlData	= NULL;//控制点
	m_nControlNum	= -1;
	m_nKnotNum		= -1;
}

BSpline::~BSpline()
{
	if (m_ControlData)
	{
		free(m_ControlData);
	}
	if (m_KnotData)
	{
		free(m_KnotData);
	}
	if (m_InputDataParameter)
	{
		free(m_InputDataParameter);
	}
}

int BSpline::FindSpan(double u)
{
	int mid;

	if (u == m_KnotData[m_nControlNum])
	{
		return m_nControlNum - 1;
	}
	if (u == m_KnotData[0])
	{
		return m_nDegree;
	}
	for (mid = m_nDegree; mid < m_nControlNum; ++mid)
	{
		if (u <= m_KnotData[mid + 1] && u > m_KnotData[mid])
		{
			break;
		}
	}
	return mid;
}

void BSpline::BasisFuns(int i, double u, double* N)
{
	N[0] = 1.0;
	double saved;
	vector<double> left(m_nDegree + 1,0);
	vector<double> right(m_nDegree + 1,0);
	for (int j = 1; j <= m_nDegree; ++j)
	{
		left[j] = u - m_KnotData[i + 1 - j];
		right[j] = m_KnotData[i + j] - u;
		saved = 0.0;
		for (int k = 0; k < j; ++k)
		{
			double temp = N[k] / (right[k + 1] + left[j - k]);
			N[k] = saved + right[k + 1] * temp;
			saved = left[j - k] * temp;
		}
		N[j] = saved;
	}
}

//---参数化
void BSpline::SplineParameterization( int nParameterizedType)
{
	m_InputDataParameter = (double*)malloc(m_nInputNum * sizeof(double));
	if (m_nInputNum < 1)
	{
		return;
	}

	switch (nParameterizedType)
	{
		case E_CHORD_PARAMETERIZED:
			{//---弦长参数化
				double TotalLength = 0;
				vector<double> vectorChordLength(m_nInputNum, 0);
				vectorChordLength[0] = 0;
				m_InputDataParameter[0] = 0;
				m_InputDataParameter[m_nInputNum - 1] = 1.0;

				for (int i = 1; i < m_nInputNum; ++i)
				{//---计算弧长
					vectorChordLength[i] = CaculateDistanceFunction(m_InputData[i], m_InputData[i - 1], 2);
					TotalLength += vectorChordLength[i];
				}

				for (int i = 1; i < m_nInputNum - 1; ++i)
				{//---计算参数
					m_InputDataParameter[i] = m_InputDataParameter[i - 1] + vectorChordLength[i] / TotalLength;
				}
			}
			break;
		case E_CENTER_PARAMETERIZED:
			{//---向心参数化
				double TotalLength = 0;
				vector<double> vectorChordLength(m_nInputNum, 0);
				vectorChordLength[0] = 0;

				m_InputDataParameter[0] = 0;
				m_InputDataParameter[m_nInputNum - 1] = 1.0;
				for (int i = 1; i < m_nInputNum; ++i)
				{
					vectorChordLength[i] = sqrt(CaculateDistanceFunction(m_InputData[i], m_InputData[i - 1], 2));
					TotalLength += vectorChordLength[i];
				}
				for (int i = 1; i < m_nInputNum - 1; ++i)
				{
					m_InputDataParameter[i] = m_InputDataParameter[i - 1] + vectorChordLength[i] / TotalLength;
				}

			}
			break;
		case E_UNIFORM_PARAMETERIZED:
			{//---均匀参数化
				for (int i = 0; i < m_nInputNum; ++i)
				{
					m_InputDataParameter[i] = 1.0 * i / (double)(m_nInputNum - 1);
				}
			}
			break;
	}
}

//---计算节点数组
bool BSpline::CalculateKnotVector()
{
	int i, j;
	if (m_nControlNum < m_nDegree + 1)
	{
		return false;
	}

	for (i = 0; i <= m_nDegree; ++i)
	{
		m_KnotData[i] = 0.0;
	}

	for (i = m_nControlNum; i < m_nKnotNum; ++i)
	{
		m_KnotData[i] = 1.0;
	}

	for (i = 1; i < m_nControlNum - m_nDegree; ++i)
	{//---UAVG技术
		int nKnotDataIdex = i + m_nDegree;
		double dTemp = 0.0;
		for (j = i; j <= i + m_nDegree - 1; ++j)
		{
			dTemp += m_InputDataParameter[j];
		}
		dTemp = dTemp / m_nDegree;

		m_KnotData[nKnotDataIdex] = dTemp;
	}
	return true;
}

//---计算控制点坐标
void BSpline::GlobalCurveInterp()
{
	int i, j, k;

	double** A = (double**)calloc(m_nControlNum, sizeof(double*));
	double** InverseA = (double**)malloc(m_nControlNum * sizeof(double*));

	for (i = 0; i < m_nControlNum; ++i)
	{
		A[i] = (double*)calloc(m_nControlNum, sizeof(double));
		InverseA[i] = (double*)malloc(m_nControlNum * sizeof(double));
	}

	for (i = 0; i < m_nControlNum; ++i)
	{
		int nSpanIndex = FindSpan(m_InputDataParameter[i]);
		BasisFuns(nSpanIndex, m_InputDataParameter[i],
			A[i] + nSpanIndex - m_nDegree);
	}

	LU_Inverse(A, m_nControlNum, InverseA);

	for (i = 0; i < m_nControlNum; ++i)
	{
		for (j = 0; j < m_nDim; ++j)
		{
			m_ControlData[i][j] = 0;
			for (k = 0; k < m_nControlNum; ++k)
			{
				m_ControlData[i][j] += InverseA[i][k] * m_InputData[k][j];
			}
		}
	}

	for (i = 0; i < m_nControlNum; ++i)
	{
		free(A[i]);
		free(InverseA[i]);
	}
	free(A);
	free(InverseA);
}

//---计算参数u对应的曲线点坐标
void BSpline::CurvePoint(double u, Vect* Point)
{
	double* dUsefulBaseFunction = (double*)malloc((m_nDegree + 1) * sizeof(double));
	int nSpanIndex = FindSpan(u);			//找到参数u时，基函数有效的节点的起始索引
	BasisFuns(nSpanIndex, u, dUsefulBaseFunction);	//计算有效基函数数组

	for (int j = 0; j < m_nDim; j++)
	{
		(*Point)[j] = 0;
		for (int i = 0; i <= m_nDegree; ++i)
		{
			(*Point)[j] += dUsefulBaseFunction[i] * m_ControlData[nSpanIndex - m_nDegree + i][j];
		}
	}
	free(dUsefulBaseFunction);
}

//---插值计算控样条曲线控制点，节点数组，样条曲线上点坐标
bool BSpline::InsertBSplineCurve(vector<CPoint>& vecControlPoint, int NumOut, Vect* out)
{
	m_nControlNum = m_nInputNum;//控制点数 = 插值点数
	m_nKnotNum    = m_nControlNum + m_nDegree + 1;
	m_ControlData = (Vect*)malloc(m_nControlNum * sizeof(Vect));
	m_KnotData    = (double*)malloc(m_nKnotNum * sizeof(double));

	if(!CalculateKnotVector())
	{
		return false;
	}
	GlobalCurveInterp();//计算控制点坐标

	double dDelta = 1.0 / ((double)NumOut - 1);

	for (int i = 0; i < NumOut-1; ++i)
	{
		double u = dDelta * i;
		Vect TempPoint;
		CurvePoint(u, &TempPoint);
		memcpy(out[i], TempPoint, sizeof(Vect));
		printf("[%f, %f, %f],\n", TempPoint[0], TempPoint[1], TempPoint[2]);
	}

	Vect TempPoint;
	CurvePoint(1, &TempPoint);
	memcpy(out[NumOut - 1], TempPoint, sizeof(Vect));

	for (int i = 0; i < m_nControlNum; ++i)
	{//---获取B样条控制点坐标
		vecControlPoint[i].x = m_ControlData[i][0];
		vecControlPoint[i].y = m_ControlData[i][1];
	}

	free(m_ControlData);
	m_ControlData	= NULL;
	free(m_KnotData);
	m_KnotData		= NULL;
	m_nControlNum	= -1;
	m_nKnotNum		= -1;

	return true;
}

int BSpline::CurveFitKnotVector()
{
	for (int i = 0; i <= m_nDegree; ++i)
	{
		m_KnotData[i] = 0.0;
	}
	for (int i = m_nControlNum; i < m_nKnotNum; ++i)
	{
		m_KnotData[i] = 1.0;
	}
	for (int i = 1; i < m_nControlNum - m_nDegree; ++i)
	{
		int id = i + m_nDegree;
		m_KnotData[id] = 1.0 * i / (m_nControlNum - m_nDegree);
	}
	return 0;
}

void BSpline::GlobalCurveFit()
{
	int i, j, k;
	Vect*	 Rk = (Vect*)malloc(m_nInputNum * sizeof(Vect));
	Vect*	 R = (Vect*)malloc(m_nControlNum * sizeof(Vect));
	double** N = (double**)calloc(m_nInputNum, sizeof(double*));
	double** NTN = (double**)calloc(m_nControlNum, sizeof(double*));
	double** InverseNTN = (double**)malloc(m_nControlNum * sizeof(double*));

	for (i = 0; i < m_nInputNum; ++i)
	{//---初始化矩阵N
		N[i] = (double*)calloc(m_nControlNum, sizeof(double));

	}
	for (i = 0; i < m_nControlNum; ++i)
	{//---初始化矩阵NTN，InverseNTN
		NTN[i] = (double*)calloc(m_nControlNum, sizeof(double));
		InverseNTN[i] = (double*)malloc(m_nControlNum * sizeof(double));
	}

	for (i = 0; i < m_nInputNum; ++i)
	{//---计算参数矩阵N
		int nSpanIndex = FindSpan(m_InputDataParameter[i]);
		BasisFuns(nSpanIndex, m_InputDataParameter[i], N[i] + nSpanIndex - m_nDegree);
	}

	for (i = 0; i < m_nControlNum; ++i)
	{//---计算系数矩阵
		for (k = 0; k < m_nDim; ++k)
		{
			R[i][k] = 0;
			for (j = 0; j < m_nInputNum; ++j)
			{
				double Nip = N[j][i];
				R[i][k] += Nip * m_InputData[j][k];
			}
		}
	}

	for (i = 0; i < m_nControlNum; ++i)
	{//---计算矩阵NTN
		for (j = 0; j < m_nControlNum; ++j)
		{
			NTN[i][j] = 0;
			for (k = 0; k < m_nInputNum; ++k)
			{
				NTN[i][j] += N[k][i] * N[k][j];
			}
		}
	}

	LU_Inverse(NTN, m_nControlNum, InverseNTN);//求矩阵NTN逆InverseNTN
	
	for (i = 0; i < m_nControlNum; ++i)
	{
		for (j = 0; j < m_nDim; ++j)
		{
			m_ControlData[i][j] = 0;
			for (k = 0; k < m_nControlNum; ++k)
			{
				m_ControlData[i][j] += InverseNTN[i][k] * R[k][j];
			}
		}

		printf("P = [%lf, %lf, %lf],\n", m_ControlData[i][0], m_ControlData[i][1], m_ControlData[i][2]);
	}

	for (i = 0; i < m_nInputNum; ++i)
	{
		free(N[i]);
	}
	free(N);

	for (i = 0; i < m_nControlNum; ++i)
	{
		free(NTN[i]);
	}
	free(NTN);

	for (i = 0; i < m_nControlNum; ++i)
	{
		free(InverseNTN[i]);
	}
	free(InverseNTN);
	free(Rk);
	free(R);
}

int BSpline::SelectControlPoints(double dEps)
{
	int i;
	bool bFlag = false;
	if (m_nInputNum - m_nDegree - 1 < 2)
	{
		return -1;
	}

	for (m_nControlNum = m_nInputNum - m_nDegree - 1; m_nControlNum > m_nDegree + 1; --m_nControlNum)
	{
		m_nKnotNum    = m_nControlNum + m_nDegree + 1;
		m_ControlData = (Vect*)malloc(m_nControlNum * sizeof(Vect));
		m_KnotData    = (double*)malloc(m_nKnotNum * sizeof(double));

		CurveFitKnotVector();
		GlobalCurveFit();

		for (i = 0; i < m_nInputNum; ++i)
		{
			Vect Point;

			CurvePoint(m_InputDataParameter[i], &Point);
			double error = CaculateDistanceFunction(Point, m_InputData[i], 2);
			if (isnormal(error) && error > dEps)
			{
				bFlag = true;
				break;
			}
		}

		free(m_ControlData);
		m_ControlData = NULL;
		free(m_KnotData);
		m_KnotData = NULL;
		if (bFlag)
		{
			break;
		}
	}
	m_nControlNum = m_nControlNum;
	m_nKnotNum = m_nControlNum + m_nDegree + 1;
	return 0;
}

int BSpline::GetBSplineCurveFitPoint(int nControlNum, int nOutDataNum, Vect* OutData)
{
	m_nControlNum = nControlNum;
	m_nKnotNum	  = m_nControlNum + m_nDegree + 1;

	if (m_nInputNum <= m_nKnotNum)
	{
		return -1;
	}
	m_ControlData = (Vect*)malloc(m_nControlNum * sizeof(Vect));
	m_KnotData    = (double*)malloc(m_nKnotNum * sizeof(double));
	CurveFitKnotVector();
	GlobalCurveFit();

	double dDelta = 1.0 / (double)nOutDataNum;
	for (int i = 0; i < nOutDataNum; ++i)
	{
		double u = dDelta * i;
		Vect TempPoint;
		CurvePoint(u, &TempPoint);//计算点坐标
		memcpy(OutData[i], TempPoint, sizeof(Vect));
		printf("[%lf, %lf, %lf],\n", TempPoint[0], TempPoint[1], TempPoint[2]);
	}
	free(m_ControlData);
	m_ControlData = NULL;
	free(m_KnotData);
	m_KnotData    = NULL;
	m_nControlNum = -1;
	m_nKnotNum    = -1;
	return 0;
}

//---获取指定误差的样条曲线点坐标
int BSpline::GetSpecifiedErroBSplineCurveFitPoint(double dEps, int NumOut, Vect* OutData)
{
	int nFlag = SelectControlPoints(dEps);

	if (nFlag)
	{
		printf("input data is too less!!!");
		return -1;
	}

	m_ControlData = (Vect*)malloc(m_nControlNum * sizeof(Vect));
	m_KnotData    = (double*)malloc(m_nKnotNum * sizeof(double));
	CurveFitKnotVector();
	GlobalCurveFit();
	double dDelta = 1.0 / (double)NumOut;

	for (int i = 0; i < NumOut; ++i)
	{
		double u = dDelta * i;
		Vect tempPoint;
		CurvePoint(u, &tempPoint);
		memcpy(OutData[i], tempPoint, sizeof(Vect));
		printf("[%lf, %lf, %lf],\n", tempPoint[0], tempPoint[1], tempPoint[2]);
	}

	free(m_ControlData);
	m_ControlData = NULL;
	free(m_KnotData);
	m_KnotData    = NULL;
	m_nControlNum = -1;
	m_nKnotNum	  = -1;
	return 0;
}


//------CubicSpline类------
CubicSpline::CubicSpline(Vect* InputData, int NumInput, int Dim, double lambda)
{
	int i, j;
	m_NumInput = NumInput;
	m_Dim = Dim;
	m_Lambda = lambda;
	m_pInputData = (Vect*)malloc(m_NumInput * sizeof(Vect));
	memcpy(m_pInputData, InputData, m_NumInput * sizeof(Vect));
	m_pStepData = (double*)malloc((m_NumInput - 1) * sizeof(double));
	double* p = (double*)malloc((m_NumInput - 1) * sizeof(double));
	Vect* q = (Vect*)malloc(m_NumInput * sizeof(Vect));
	a = (Vect*)malloc((m_NumInput - 1) * sizeof(Vect));
	b = (Vect*)malloc((m_NumInput - 1) * sizeof(Vect));
	c = (Vect*)malloc((m_NumInput - 1) * sizeof(Vect));
	d = (Vect*)malloc((m_NumInput - 1) * sizeof(Vect));

	SplineParameterization();



	//m_pStepData[0] = m_pParamInputData[1] - m_pParamInputData[0];//caculateDistancef(m_pInputData[1], m_pInputData[0], dd);
	//for (i = 1; i < m_NumInput - 1; ++i)
	//{
	//	m_pStepData[i] = m_pParamInputData[i + 1] - m_pParamInputData[i]; //caculateDistancef(m_pInputData[i + 1], m_pInputData[i], dd);
	//	p[i] = 2 * (m_pParamInputData[i + 1] - m_pParamInputData[i - 1]);
	//	for (j = 0; j < m_Dim; ++j)
	//	{
	//		q[i][j] = 3 * (m_pInputData[i + 1][j] - m_pInputData[i][j]) / m_pStepData[i] - 3 * (m_pInputData[i][j] - m_pInputData[i - 1][j]) / m_pStepData[i - 1];
	//	}
	//}

	///*Gaussian Elimination*/
	//for (i = 2; i < m_NumInput - 1; ++i)
	//{
	//	p[i] = p[i] - m_pStepData[i - 1] * m_pStepData[i - 1] / p[i - 1];
	//	for (j = 0; j < m_Dim; ++j)
	//	{
	//		q[i][j] = q[i][j] - q[i - 1][j] * m_pStepData[i - 1] / p[i - 1];
	//	}
	//}
	///*Backsubtitution*/
	//for (j = 0; j < m_Dim; ++j)
	//{
	//	b[m_NumInput - 2][j] = q[m_NumInput - 2][j] / p[m_NumInput - 2];
	//}
	//for (i = 3; i < m_NumInput; ++i)
	//{
	//	for (j = 0; j < m_Dim; ++j)
	//	{
	//		b[m_NumInput - i][j] = (q[m_NumInput - i][j] - m_pStepData[m_NumInput - i] * b[m_NumInput - i + 1][j]) / p[m_NumInput - i];
	//	}
	//}

	///*spline parameters*/
	//double ddy = (3.0 * m_pStepData[0]);
	//for (j = 0; j < m_Dim; ++j)
	//{
	//	a[0][j] = b[1][j] / ddy;
	//	b[0][j] = 0;
	//	c[0][j] = (m_pInputData[1][j] - m_pInputData[0][j]) / m_pStepData[0] - b[1][j] * m_pStepData[0] / 3;
	//	d[0][j] = m_pInputData[0][j];
	//	//b[m_NumInput - 2][j] = 0;
	//}
	//printf("[%lf, %lf, %lf, %lf],\n", b[1][0], b[1][1], b[1][2], m_pStepData[0]);
	//printf("[%lf, %lf, %lf],\n", a[0][0], a[0][1], a[0][2]);
	//for (i = 1; i < m_NumInput - 1; ++i)
	//{
	//	for (j = 0; j < m_Dim; ++j)
	//	{
	//		a[i][j] = (b[i + 1][j] - b[i][j]) / (2 * m_pStepData[i]);
	//		c[i][j] = (m_pInputData[i + 1][j] - m_pInputData[i][j]) / m_pStepData[i] - a[i][j] * m_pStepData[i] * m_pStepData[i] - b[i][j] * m_pStepData[i];//(b[i][j] + b[i - 1][j])*m_pStepData[i - 1] + c[i - 1][j];
	//		d[i][j] = m_pInputData[i][j];
	//	}
	//}


	double* r = (double*)malloc((m_NumInput - 1) * sizeof(double));
	double* f = (double*)malloc((m_NumInput - 1) * sizeof(double));
	double* u = (double*)malloc((m_NumInput - 1) * sizeof(double));
	double* v = (double*)malloc((m_NumInput - 1) * sizeof(double));
	double* w = (double*)malloc((m_NumInput - 1) * sizeof(double));
	double sigma = 1.0;
	double mu = 2 * (1 - m_Lambda) / (3 * m_Lambda);
	m_pStepData[0] = m_pParamInputData[1] - m_pParamInputData[0];
	r[0] = 3 / m_pStepData[0];
	for (i = 1; i < m_NumInput - 1; ++i)
	{
		m_pStepData[i] = m_pParamInputData[i + 1] - m_pParamInputData[i];
		r[i] = 3 / m_pStepData[i];
		f[i] = -(r[i - 1] + r[i]);
		p[i] = 2 * (m_pParamInputData[i + 1] - m_pParamInputData[i - 1]);
		for (j = 0; j < m_Dim; ++j)
		{
			q[i][j] = 3 * (m_pInputData[i + 1][j] - m_pInputData[i][j]) / m_pStepData[i] - 3 * (m_pInputData[i][j] - m_pInputData[i - 1][j]) / m_pStepData[i - 1];
		}
	}
	u[0] = 0.0;
	v[0] = 0.0;
	w[0] = 0.0;
	for (i = 1; i < m_NumInput - 1; ++i)
	{
		u[i] = r[i - 1] * r[i - 1] * sigma + f[i] * f[i] * sigma + r[i] * r[i] * sigma;
		u[i] = mu * u[i] + p[i];
		v[i] = f[i] * r[i] * sigma + r[i] * f[i + 1] * sigma;
		v[i] = mu * v[i] + m_pStepData[i];
		w[i] = mu * r[i] * r[i + 1] * sigma;

	}

	Quincunx(m_NumInput, u, v, w, q);

	/*spline parameters*/
	for (j = 0; j < m_Dim; ++j)
	{
		d[0][j] = m_pInputData[0][j] - mu * r[0] * q[1][j] * sigma;
		double dx = m_pInputData[1][j] - mu * (f[1] * q[1][j] + r[1] * q[2][j]) * sigma;

		a[0][j] = q[1][j] / (3.0 * m_pStepData[0]);
		b[0][j] = 0;
		c[0][j] = (dx - d[0][j]) / m_pStepData[0] - q[1][j] * m_pStepData[0] / 3;
		//r[0] = 0;
	}
	for (i = 1; i < m_NumInput - 1; ++i)
	{
		for (j = 0; j < m_Dim; ++j)
		{
			a[i][j] = (q[i + 1][j] - q[i][j]) / (3 * m_pStepData[i]);
			b[i][j] = q[i][j];
			c[i][j] = (q[i][j] + q[i - 1][j]) * m_pStepData[i - 1] + c[i - 1][j];
			d[i][j] = r[i - 1] * q[i - 1][j] + f[i] * q[i][j] + r[i] * q[i + 1][j];
			d[i][j] = m_pInputData[i][j] - mu * d[i][j] * sigma;

		}
	}

	free(r);
	free(f);
	free(u);
	free(v);
	free(w);
	free(p);
	free(q);

}

CubicSpline::~CubicSpline()
{
	free(m_pParamInputData);
	free(m_pStepData);
	free(m_pInputData);
	free(a);
	free(b);
	free(c);
	free(d);
}

void CubicSpline::Quincunx(int n, double* u, double* v, double* w, Vect* q)
{
	int i, j;
	u[0] = 0;
	/*factorisation*/
	//u[1] = u[1] - u[0] * v[0] * v[0];
	v[1] = v[1] / u[1];
	w[1] = w[1] / u[1];
	for (i = 2; i < n - 1; ++i)
	{
		u[i] = u[i] - u[i - 2] * w[i - 2] * w[i - 2] - u[i - 1] * v[i - 1] * v[i - 1];
		v[i] = (v[i] - u[i - 1] * v[i - 1] * w[i - 1]) / u[i];
		w[i] = w[i] / u[i];
	}

	/*forward substitution*/

	for (i = 2; i < n - 1; ++i)
	{
		for (j = 0; j < m_Dim; ++j)
		{
			q[i][j] = q[i][j] - v[i - 1] * q[i - 1][j] - w[i - 2] * q[i - 2][j];
		}
	}

	for (i = 1; i < n - 1; ++i)
	{
		for (j = 0; j < m_Dim; ++j)
		{
			q[i][j] = q[i][j] / u[i];
		}
	}

	/*back substitution*/
	for (j = 0; j < m_Dim; ++j)
	{
		q[0][j] = 0.0;
		q[n - 1][j] = 0;
	}
	for (i = n - 3; i > 0; --i)
	{
		for (j = 0; j < m_Dim; ++j)
		{

			q[i][j] = q[i][j] - v[i] * q[i + 1][j] - w[i] * q[i + 2][j];
		}
	}

}

int CubicSpline::FindSpan(double u)
{
	int mid;

	for (mid = 0; mid < m_NumInput - 1; ++mid)
	{
		if (u < m_pParamInputData[mid + 1] && u >= m_pParamInputData[mid])
		{
			break;
		}
	}
	return mid;
}

void CubicSpline::CulicSpline_CurveInterp(int NumOut, Vect* OutData)
{
	int i, j;
	double delta = 1.0 / NumOut;

	for (i = 0; i < NumOut; ++i)
	{
		double u = i * delta * m_ArcLen;
		int span = FindSpan(u);
		double sx = m_pParamInputData[span];
		double cx = u - sx;

		for (j = 0; j < m_Dim; ++j)
		{
			OutData[i][j] = a[span][j] * cx * cx * cx
				+ b[span][j] * cx * cx + c[span][j] * cx + d[span][j];
		}
	}
}

//---弦长参数化
void CubicSpline::SplineParameterization()
{
	double dist = 0;
	m_pParamInputData = (double*)malloc(m_NumInput * sizeof(double));
	m_pParamInputData[0] = 0;
	for (int i = 1; i < m_NumInput; ++i)
	{
		dist = CaculateDistanceFunction(m_pInputData[i], m_pInputData[i - 1], nDimention);
		m_pParamInputData[i] = m_pParamInputData[i - 1] + dist;
	}
	m_ArcLen = m_pParamInputData[m_NumInput - 1];

}


//------函数------
//---获取CubicSplinePoint
int GetCubicSplinePoint(vector<CPoint> vecInsertPoint, vector<CPoint>& vecBSlinePoint,
	int nBSplinePointNum, int nDegree)
{
	int dim = nDimention;
	int nInsertPoint = (int)vecInsertPoint.size();
	if(nInsertPoint <= nDegree)
	{
		vecBSlinePoint = vecInsertPoint;
		return 0;
	}

	int NumInput = nInsertPoint;
	int NumControl = nInsertPoint;
	int NumOut = nBSplinePointNum;
	double eps = 50.0;
	Vect* data = (Vect*)malloc(NumInput * sizeof(Vect));
	Vect* out = (Vect*)malloc(NumOut * sizeof(Vect));
	vecBSlinePoint.resize(NumOut);
	
	for (int i = 0; i < NumInput; ++i)
	{
		data[i][0] = (vecInsertPoint[i].x);
		data[i][1] = (vecInsertPoint[i].y);
		data[i][2] = 0;
	}
	
	CubicSpline* cspline = new CubicSpline(data, NumInput, dim, 0.00001);
	cspline->CulicSpline_CurveInterp(NumOut, out);
	for (int i = 0; i < NumOut; ++i)
	{
		vecBSlinePoint[i].x = (out[i][0]);
		vecBSlinePoint[i].y = (out[i][1]);
		cout << i << "	:" << vecBSlinePoint[i].x << "	," << vecBSlinePoint[i].y;
	}

	free(data);
	free(out);
	return 1;
}

//---获取BSplinePoint、控制点
bool GetBSplinePoint(vector<CPoint> vecInsertPoint, vector<CPoint>& vecControlPoint,
	vector<CPoint>& vecBSlinePoint, int nBSplinePointNum, int nDegree)//输入插值点得到BSpline上数据点
{
	int   nInputNum = (int)vecInsertPoint.size();
	int	  nDimetion = 3;
	Vect* InputData;
	Vect* OutData;

	InputData = (Vect*)malloc(nInputNum * sizeof(Vect));
	OutData   = (Vect*)malloc(nBSplinePointNum * sizeof(Vect));

	vecControlPoint.resize(nInputNum);
	vecBSlinePoint.resize(nBSplinePointNum);

	for(int i =0; i < nInputNum; ++i)
	{//---初始化插值数据
		InputData[i][0] = vecInsertPoint[i].x;
		InputData[i][1] = vecInsertPoint[i].y;
		InputData[i][2] = 0;
	}

	BSpline bSpline(InputData, nInputNum, nDimetion, nDegree);
	bool bIfInserted = bSpline.InsertBSplineCurve(vecControlPoint,nBSplinePointNum, OutData);

	if(!bIfInserted)
	{
		return false;
	}
	for(int i = 0; i < nBSplinePointNum; ++i)
	{//---获取B样条点坐标
		vecBSlinePoint[i].x = OutData[i][0];
		vecBSlinePoint[i].y = OutData[i][1];
	}
	
	free(InputData);
	free(OutData);
	return true;
}