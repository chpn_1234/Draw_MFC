﻿#pragma once
#include<vector>
#include<string>
#include<Windows.h>
using namespace std;


class MyEdit :public CEdit
{
public:
    MyEdit();
    ~MyEdit();

public:
    DECLARE_MESSAGE_MAP()
  
        virtual BOOL PreTranslateMessage( MSG *pMsg );
};




class DlgShowSortAlgorithm : public CDialogEx
{
	DECLARE_DYNAMIC(DlgShowSortAlgorithm)

public:
	DlgShowSortAlgorithm(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~DlgShowSortAlgorithm();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DLG_SHOW_SORT_ALGORITHM};
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()

public:
	CString m_input_str;
    MyEdit   m_input_control;
	afx_msg void OnClickedButton1();
	afx_msg void OnClickedButton2();
	afx_msg void OnClickedButton3();
	afx_msg void OnClickedButton4();
	afx_msg void OnClickedButton5();
	
	int CHOOSE_SORT(vector<int>& arr);
	void DRAW_ARR_RECT(vector<int>& arr,int index);
	void BUBBLE_SORT(vector<int>& arr);
	void INSERT_SORT(vector<int>& arr);
	void GET_ARR(vector<int>& arr);

	
	int m_button1;
	int m_button2;
	int m_button3;
	afx_msg void OnChangeEdit1();
  
    afx_msg void OnBnClickedSplit1();
    CButton m_ctrlSpinButton;
};
