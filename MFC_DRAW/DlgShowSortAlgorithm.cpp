﻿// CSORT_SHOW.cpp: 实现文件
//

#include "pch.h"
#include "MFC_DRAW.h"
#include "DlgShowSortAlgorithm.h"
#include "afxdialogex.h"
#include <vector>
#include <string>
#include <Windows.h>
#include "XSLEEP.h"





MyEdit::MyEdit( ): CEdit()
{
 
}



MyEdit::~MyEdit()
{
}




BEGIN_MESSAGE_MAP( MyEdit, CEdit )
    
END_MESSAGE_MAP()




BOOL MyEdit::PreTranslateMessage( MSG *pMsg )
{
    /*SetWindowText( _T( "chenpeng" ) );*/
    //响应标签页切换的快捷键  
    switch( pMsg->message )
    {
    case WM_PASTE:
        
        return TRUE;
    }

    return CEdit::PreTranslateMessage( pMsg );
}








using namespace std;

// CSORT_SHOW 对话框

IMPLEMENT_DYNAMIC(DlgShowSortAlgorithm, CDialogEx)

DlgShowSortAlgorithm::DlgShowSortAlgorithm(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_DIALOG1, pParent)
	, m_input_str(_T(""))
{

	m_button1 = 200;
	m_button2 = 200;
	m_button3 = 200;
}

DlgShowSortAlgorithm::~DlgShowSortAlgorithm()
{
}

void DlgShowSortAlgorithm::DoDataExchange(CDataExchange* pDX)
{
    CDialogEx::DoDataExchange( pDX );
    DDX_Text( pDX, IDC_EDIT1, m_input_str );
    DDX_Control( pDX, IDC_EDIT1, m_input_control );
    DDX_Control( pDX, IDC_SPLIT1, m_ctrlSpinButton );
}


BEGIN_MESSAGE_MAP(DlgShowSortAlgorithm, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON1, &DlgShowSortAlgorithm::OnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &DlgShowSortAlgorithm::OnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &DlgShowSortAlgorithm::OnClickedButton3)
	ON_BN_CLICKED(IDC_BUTTON4, &DlgShowSortAlgorithm::OnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON5, &DlgShowSortAlgorithm::OnClickedButton5)
	ON_EN_CHANGE(IDC_EDIT1, &DlgShowSortAlgorithm::OnChangeEdit1)
 
    ON_BN_CLICKED( IDC_SPLIT1, &DlgShowSortAlgorithm::OnBnClickedSplit1 )
END_MESSAGE_MAP()


// CSORT_SHOW 消息处理程序


void DlgShowSortAlgorithm::OnClickedButton1()
{
	// TODO: 在此添加控件通知处理程序代码
	m_button1 = 50;
	m_button2 = 0;
	m_button3 = 0;
	

		vector<int> arr;
		GET_ARR(arr);
		CHOOSE_SORT(arr);


}


void DlgShowSortAlgorithm::OnClickedButton2()
{
	// TODO: 在此添加控件通知处理程序代码
	m_button1 = 0;
	m_button2 = 50;
	m_button3 = 0;



		vector<int> arr;
		GET_ARR(arr);
		BUBBLE_SORT(arr);

}


void DlgShowSortAlgorithm::OnClickedButton3()
{
	// TODO: 在此添加控件通知处理程序代码

	m_button1 = 0;
	m_button2 = 0;
	m_button3 = 50;
		vector<int> arr;
		GET_ARR(arr);
		INSERT_SORT(arr);
}


void DlgShowSortAlgorithm::OnClickedButton4()
{
	// TODO: 在此添加控件通知处理程序代码
}


void DlgShowSortAlgorithm::OnClickedButton5()
{
	// TODO: 在此添加控件通知处理程序代码
}


void DlgShowSortAlgorithm::DRAW_ARR_RECT(vector<int>& arr,int index)
{
	// TODO: 在此处添加实现代码.
	int n= arr.size();

	int max_num=0;
	for (int i = 0; i < arr.size(); ++i) {
		if (arr[i] > max_num) {
			max_num = arr[i];
		}
	}

	//获取对话框rect 参数
	CWnd* pwnd = GetDlgItem(IDC_PICTURE1);
	CRect rect_dlg;
	pwnd->GetClientRect(rect_dlg);
	int dlg_width = rect_dlg.Width();
	int dlg_height = rect_dlg.Height();

	//获取画布
	CDC* pdc(pwnd->GetDC());
	//创建画刷
	CBrush brush1(RGB(0,0,0));//黑色画刷
	CBrush brush2(RGB(255,255, 255));//白色画刷
	CBrush brush3(RGB(255, 0, 0));//红色画刷


	//计算可视画数据矩形框的高宽，以及坐标
	vector<CRect> rects;
	vector<CRect> rects2;

	int rect_w = (dlg_width - 5) / n;
	for (int i = 0; i < n; ++i) {
		int rect_h = dlg_height - (dlg_height - 30) / max_num * arr[i];
		CRect rect1(CPoint(i * rect_w, rect_h), CPoint(i * rect_w + rect_w/2, dlg_height));
		CRect rect2(CPoint(i * rect_w, rect_h-20), CPoint(i * rect_w + rect_w / 2, rect_h));
		rects.push_back(rect1);
		rects2.push_back(rect2);

	}

	//先画一个白色背景覆盖前一次比较的矩形框
	pdc->FillRect(rect_dlg, &brush2);

	//画可视化数据矩形框
	for (int i = 0; i < n; ++i) {
		if (i == index) {
			pdc->FillRect(rects[i], &brush3);

		}
		else{
		pdc->FillRect(rects[i], &brush1);
		}
		//添加显示数字
		//设置字体颜色
		pdc->SetBkMode(TRANSPARENT);
		//设置字体颜色
		pdc->SetTextColor(RGB(0, 255, 0));
		//显示文本，居中显示
		CString str;
		str.Format(_T("%d"), arr[i]);
		pdc->DrawText(str, rects2[i],
			DT_SINGLELINE | DT_CENTER | DT_VCENTER);
	}

}


int DlgShowSortAlgorithm::CHOOSE_SORT(vector<int>& arr)
{
	// TODO: 在此处添加实现代码.
	int n = arr.size();
	if (n == 0 || n == 1) {
		return 0;
	}
	DRAW_ARR_RECT(arr, -1);

	for (int i = 0; i < n - 1; ++i) {


		int k = i;
		for (int j = i + 1; j < n; ++j) {
			if (arr[k] < arr[j]) {
				k = j;
			}
		}
		DRAW_ARR_RECT(arr, k);
		XSleep(m_button1);
		int temp = arr[k];
		arr[k] = arr[i];
		arr[i] = temp;
		DRAW_ARR_RECT(arr, i);
		XSleep(m_button1);
	}

	return 0;
}

void DlgShowSortAlgorithm::BUBBLE_SORT(vector<int> &arr)
{
	// TODO: 在此处添加实现代码.
	int n = arr.size();
	if (n == 0 || n == 1) {
		return ;
	}
	DRAW_ARR_RECT(arr, -1);

	for (int i = 0; i < n - 1; ++i) {

		for (int j = 0; j < n - i - 1; ++j) {
			if (arr[j] < arr[j + 1]) {
				DRAW_ARR_RECT(arr, j+1);
				XSleep(m_button2);
				int temp = arr[j + 1];
				arr[j + 1] = arr[j];
				arr[j] = temp;
				DRAW_ARR_RECT(arr, j);
				XSleep(m_button2);
			}
		}
	

	}
}

void DlgShowSortAlgorithm::INSERT_SORT(vector<int>& arr)
{
	// TODO: 在此处添加实现代码.
	int n = arr.size();
	if (n == 0 || n == 1) {
		return ;
	}
	DRAW_ARR_RECT(arr, -1);

	for (int i = 1; i < n; ++i) {
		int temp = arr[i];//注意arr[i]会改变，所以必须提前保存值
		for (int j = i - 1; j >= 0; j = j - 1) {
			if (arr[j] > temp) {
				break;
			}
			else {
				DRAW_ARR_RECT(arr, j+1);
				XSleep(m_button3);
				arr[j + 1] = arr[j];
				arr[j] = temp;
				DRAW_ARR_RECT(arr, j);
				XSleep(m_button3);
			}
		}
		
		
	}
}


void DlgShowSortAlgorithm::GET_ARR( vector<int>& arr)
{
	// TODO: 在此处添加实现代码.
	//将str转化为string类型
	CString  str;
	m_input_control.GetWindowTextW(str);
	string s;
	//方法1
	/*wstring ws(str);
	s.assign(ws.begin(), ws.end());*/
	//方法2
	s = CT2A(str.GetString());

	int index_start = 0;
	int first = 0;
	for (int i = 0; i < s.length(); ++i) {
		if (first == 0) {
			if ('0' <= s[i] && s[i] <= '9') {
				first = 1;
				index_start = i;
			}
		}
		else {

			if (s[i] < '0' || s[i] > '9') {
				first = 0;
				int num = stoi(s.substr(index_start, i - index_start));
				arr.push_back(num);
			}
		}
	}
}


void DlgShowSortAlgorithm::OnChangeEdit1()
{
	// TODO:  如果该控件是 RICHEDIT 控件，它将不
	// 发送此通知，除非重写 CDialogEx::OnInitDialog()
	// 函数并调用 CRichEditCtrl().SetEventMask()，
	// 同时将 ENM_CHANGE 标志“或”运算到掩码中。

	// TODO:  在此添加控件通知处理程序代码
	m_input_str = "123";
	m_input_control.UpdateData();

	
}



void DlgShowSortAlgorithm::OnBnClickedSplit1()
{
    // TODO: 在此添加控件通知处理程序代码
}
