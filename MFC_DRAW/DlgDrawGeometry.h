﻿#pragma once
#include <vector>
#include "BSpline.h"
using namespace std;
// CMDraw_Dlg 对话框

class DlgDrawGeometry : public CDialogEx
{
	DECLARE_DYNAMIC(DlgDrawGeometry)

public:
	DlgDrawGeometry(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~DlgDrawGeometry();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_FIGURE_DLG };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	CListBox m_list1;
	afx_msg void OnSelchangeList1();
	afx_msg void OnDblclkList1();
	afx_msg void OnBnClickedStraightLine();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg void OnBnClickedButtonClear();
	afx_msg void OnBnClickedBSpline();

	//---变量
	CStatic m_draw_board;
	CPoint	m_mousel_down;
	bool	m_mousel_isdown;
	CPoint	m_mousel_up;
	CPoint	m_mouse_move;
	CPoint	m_correct_value;
	CRect	m_draw_board_client;
	CRect	m_draw_board_window;
	int		m_draw_number = 0;				//绘制次数
	CTreeCtrl	   m_figure_tree;
	vector<CPoint> m_vecBSplineInsertPoint;	//插值绘制B样条曲线的数据点
	vector<CPoint> m_vecBSplineControlPoint;//插值绘制B样条曲线的数据点
	vector<CPoint> m_vecBSplinePoint;		//B样条曲线上的点
	bool	m_BSplineButtonIsDown = false;	//是否点击BSline按钮
	int		m_nDegree = 3;					//BSpline 阶数；

	//---函数
	void Listbox_Practice();
	void Get_Correct_Value();
	void Draw_Figure();
	void Initial_Tree();
	void Creat_Figure_From_XML();
	void Write_XML();

	void DrawBSpline();					//绘制B样条曲线
	void UpdateDegree();				//更新m_nDegree
};
