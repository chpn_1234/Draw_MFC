﻿
// MFC_DRAWView.h: CMFCDRAWView 类的接口
//

#pragma once


class CMFCDRAWView : public CView
{
protected: // 仅从序列化创建
	CMFCDRAWView() noexcept;
	DECLARE_DYNCREATE(CMFCDRAWView)

// 特性
public:
	CMFCDRAWDoc* GetDocument() const;

// 操作
public:

// 重写
public:
	virtual void OnDraw(CDC* pDC);  // 重写以绘制该视图
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);

// 实现
public:
	virtual ~CMFCDRAWView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 生成的消息映射函数
protected:
	afx_msg void OnFilePrintPreview();
	afx_msg void OnRButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnPaint();
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnRButtonDown(UINT nFlags, CPoint point);
private:
	int m_right_down;
	CPoint m_pt_origin;
public:
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
private:
	CPoint m_last_point;
	BOOL m_mouse_bool;
public:
//	afx_msg void OnIdrSort();
	afx_msg void OnOnsortDlg();
	afx_msg void OnOndrawDlg();
	afx_msg void OnOpenDlgFindLeaks();
};

#ifndef _DEBUG  // MFC_DRAWView.cpp 中的调试版本
inline CMFCDRAWDoc* CMFCDRAWView::GetDocument() const
   { return reinterpret_cast<CMFCDRAWDoc*>(m_pDocument); }
#endif

