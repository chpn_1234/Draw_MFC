#pragma once
#include <vector>
#include <stdio.h>
using namespace std;

#define nDimention 3
typedef double Vect[nDimention];

void	SVDslove(double** A, int m, int n, double** inverse);			//矩阵奇异分解
void	Decompose_LU(double** M, int dim, double** L, double** U);		//分解矩阵M
void	LU_Inverse(double** data, int dim, double** inverse);			//求矩阵逆
double	CaculateDistanceFunction(double* a, double* b, int size);		//计算啊a、b两点距离

enum EParameterizedType
{
	E_CHORD_PARAMETERIZED,		//弦长参数化
	E_CENTER_PARAMETERIZED,		//向心参数化
	E_UNIFORM_PARAMETERIZED		//均匀参数化
};


class BSpline
{
public:
	BSpline(Vect* InputData, int nInputNum, int nDim, int nDegree);
	~BSpline();
	int  GetBSplineCurveFitPoint(int nControlNum, int nOutNum, Vect* OutData);			//获取样条曲线点坐标
	int  GetSpecifiedErroBSplineCurveFitPoint(double dEps, int nOutNum, Vect* OutData);	//获取指定误差的样条曲线点坐标
	bool InsertBSplineCurve(vector<CPoint>& vecControlPoint,int nOutNum, Vect* outData);

	int		m_nDim;					//坐标维度
	int		m_nControlNum;			//控制点数
	int		m_nInputNum;			//输入点数
	int		m_nKnotNum;				//节点数
	int		m_nDegree;				//阶数
	double* m_KnotData;				//节点数组
	double* m_InputDataParameter;	//输入数据参数数组
	Vect*	m_ControlData;			//控制点
	Vect*	m_InputData;			//输入点

private:
	int  FindSpan(double u);						//返回参数u的所在的区间
	void BasisFuns(int i, double u, double* N);		//计算基函数
	void SplineParameterization( int nParameterizedType = E_UNIFORM_PARAMETERIZED);//参数化
	bool CalculateKnotVector();						//计算节点数组
	int  CurveFitKnotVector();						//拟合节点数组
	int  SelectControlPoints(double dEps);			//判断当前控制点是否满足误差需求
	void GlobalCurveFit();							//曲线拟合
	void GlobalCurveInterp();
	void CurvePoint(double s, Vect* Point);
};


class CubicSpline
{
public:
	CubicSpline(Vect* InputData, int NumInput, int Dim, double lambda);
	~CubicSpline();
	void CulicSpline_CurveInterp(int NumOut, Vect* OutData);
private:
	int  FindSpan(double u);
	void SplineParameterization();
	void Quincunx(int n, double* u, double* v, double* w, Vect* q);

	double	m_ArcLen;
	int		m_NumInput;
	int		m_NumKnot;
	int		m_Dim;
	double	m_Lambda;

	double* m_pStepData;		//节点
	double* m_pParamInputData;	//参数化输入
	Vect*	m_pInputData;

	Vect* a;
	Vect* b;
	Vect* c;
	Vect* d;
};

int GetCubicSplinePoint(vector<CPoint> vecInsertPoint, vector<CPoint>& vecBSlinePoint,
	int nBSplinePointNum = 10, int nDegree = 3);//输入插值点得到BSpline上数据点

bool GetBSplinePoint(vector<CPoint> vecInsertPoint, vector<CPoint>& vecControlPoint, 
	vector<CPoint>& vecBSlinePoint,int nBSplinePointNum = 10, int nDegree = 3);//输入插值点得到BSpline上数据点