﻿#pragma once


// DlgFianLeaks 对话框

class DlgFindLeaks : public CDialogEx
{
	DECLARE_DYNAMIC(DlgFindLeaks)

public:
	DlgFindLeaks(CWnd* pParent = nullptr);   // 标准构造函数
	virtual ~DlgFindLeaks();

// 对话框数据
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_DLG_FIND_LEAKS };
#endif

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButtonStartFindLeaks();



	CButton m_ctrStartFindLeaks;		//	开始查找内存泄露按钮
	CEdit	m_ctrShowLeaks;				//	显示内存泄露编辑框

};
